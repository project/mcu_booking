<?php
// $Id$

/**
 * @file
 * Lister part of the mcu booking module
 */

/**
 * This function listing the user's all reserved and ongoing conferences and the user's meeting rooms.
 *
 * @return
 *   Conferences list in HTML format.
 */
function _mcu_booking_list() {
  global $_mcu_booking_mcu, $_mcu_booking_web_user_id;
  $output = '';

  $attributes = array('class' => 'mcu_booking');
  $header = array(t('Name'), t('Dial-in number'), t('Start time'), t('Finish'), t('Duration'), t('Actions'));
  $admin_header = $header;
  $admin_header[] = t('Owner');

  //list reservations
  $res = $_mcu_booking_mcu->mcu_reservation_list();
  if (!empty($res)) {
    $rows = array();
    foreach ($res as $data) {
      $res_details = $_mcu_booking_mcu->mcu_reservation_get_details($data['id']);
      if ($res_details['web_reserved_uid'] == $_mcu_booking_web_user_id) {
        $rows[] = array(
            $data['display_name'],
            l($data['dial-in_number'], 'reservation_details/'. $data['id'] .'/view'),
            format_date($data['start_time'], 'small'),
            format_date($data['end_time'], 'small'),
            $data['duration'],
            l(t('Modify'), 'reservation_details/'. $data['id'] .'/modify') .'<br />'. l(t('Delete'), 'reservation_details/'. $data['id'] .'/delete'),
        );
      }
      elseif (user_access('mcu admin')) {
        $name = iir_user_name($res_details['web_reserved_uid']);
        $admin_rows[] = array(
            $data['display_name'],
            l($data['dial-in_number'], 'reservation_details/'. $data['id'] .'/view'),
            format_date($data['start_time'], 'small'),
            format_date($data['end_time'], 'small'),
            $data['duration'],
            l(t('Modify'), 'reservation_details/'. $data['id'] .'/modify') .'<br />'. l(t('Delete'), 'reservation_details/'. $data['id'] .'/delete'),
            $name,
        );
      }
    }
    if (!empty($rows)) {
      $output .= t('<h2>Reserved conference</h2>');
      $output .= theme_table($header, $rows, $attributes);
    }

    if (!empty($admin_rows)) {
      $output .= t('<h2>Other Users Reserved Conference</h2>');
      $output .= theme_table($admin_header, $admin_rows, $attributes);
    }
  }

  //list ongoing conferences
  $ongoing = $_mcu_booking_mcu->mcu_ongoing_list();
  if (!empty($ongoing)) {
    $rows = array();
    foreach ($ongoing as $data) {
      $conf_details = $_mcu_booking_mcu->mcu_ongoing_get_details($data['id']);
      if ($conf_details['web_reserved_uid'] == $_mcu_booking_web_user_id) {
        $rows[] = array(
            $data['display_name'],
            l($data['dial-in_number'], 'ongoing_details/'. $data['id'] .'/view/'),
            format_date($data['start_time'], 'small'),
            format_date($data['end_time'], 'small'),
            $data['duration'],
            l(t('Modify'), 'ongoing_details/'. $data['id'] .'/modify') .'<br />'. l(t('Delete'), 'ongoing_details/'. $data['id'] .'/delete'),
        );
      }
    elseif (user_access('mcu admin')) {
        $name = iir_user_name($res_details['web_reserved_uid']);
        $admin_rows[] = array(
            $data['display_name'],
            l($data['dial-in_number'], 'ongoing_details/'. $data['id'] .'/view'),
            format_date($data['start_time'], 'small'),
            format_date($data['end_time'], 'small'),
            $data['duration'],
            l(t('Modify'), 'ongoing_details/'. $data['id'] .'/modify') .'<br />'. l(t('Delete'), 'reservation_details/'. $data['id'] .'/delete'),
            $name,
        );
      }
    }

    if (!empty($rows)) {
      $output .= t('<h2>Ongoing conference</h2>');
      $output .= theme_table($header, $rows, $attributes);
    }

    if (!empty($admin_rows)) {
      $output .= t('<h2>Other Users Ongoing Conference</h2>');
      $output .= theme_table($admin_header, $admin_rows, $attributes);
    }
  }

 //list meeting rooms
  $ongoing = $_mcu_booking_mcu->mcu_meeting_room_list();
  if (!empty($ongoing)) {
    $rows = array();
    foreach ($ongoing as $data) {
      $meeting_room_details = $_mcu_booking_mcu->mcu_meeting_room_get_details($data['id']);
      if ($meeting_room_details['web_reserved_uid'] == $_mcu_booking_web_user_id OR user_access('mcu admin')) {
        $rows[] = array(
            $data['display_name'],
            l($data['dial-in_number'], 'meeting_room_details/'. $data['id'] .'/view/'),
            l(t('Modify'), 'meeting_room_details/'. $data['id'] .'/modify') .'<br />'. l(t('Delete'), 'meeting_room_details/'. $data['id'] .'/delete'),
        );
      }
    }
    if (!empty($rows)) {
      $meeting_room_header = $header;
      unset($meeting_room_header[2]);
      unset($meeting_room_header[3]);
      unset($meeting_room_header[4]);
      $output .= t('<h2>Meeting room</h2>');
      $output .= theme_table($meeting_room_header, $rows, $attributes);
    }
  }

  if (empty($output)) {
    $res_link = l(t('here'), 'mcu_booking/reservation');
    return t('You don\'t have any reservation or ongoing conference. Please click !here for conference reservation!', array('!here' => $res_link));
  }
  else {
    return $output;
  }
}

/**
 * History viewer
 *
 * @return
 *   History in HTML format.
 */
function _mcu_booking_history() {
  global  $_mcu_booking_web_user_id;
  $rows = array();
  $attributes = array('class' => 'mcu_booking');

  $headings = array(t('Type of conference'), t('Name'), t('Dial-in number'), t('Start'), t('Finish'), t('Duration'), t('Status'));
  foreach ($headings as $head) {
    $header[] = array('data' => $head);
  }

  db_set_active('iir2');
    $result = db_query("
      SELECT id, ember_id, conf_type, conf_name, conf_id, conf_num_id, conf_media,
             conf_entry_passwd, conf_chair_passwd, conf_video_session, conf_transfer_rate,
             conf_layout, conf_start_time, conf_duration, conf_status, conf_status_time, conf_comment
      FROM {mcu_reservation}
      WHERE ember_id = %d AND (conf_start_time < '%s' OR conf_status = 'deleted') GROUP BY conf_num_id ORDER BY conf_start_time DESC",  $_mcu_booking_web_user_id, date('Y-m-d H:i:s'));
  db_set_active();

  while ($data = db_fetch_array($result)) {
    $start_time = strtotime($data['conf_start_time']);
    $d = explode(':', $data['conf_duration']);
    $end_time = date('Y-m-d H:i:s', strtotime('+'. $d[0] .' hours '. $d[1] .' minutes', $start_time));

    $rows[] = array(
        $data['conf_type'],
        $data['conf_comment'],
        '003610030'. $data['conf_name'],
        $data['conf_start_time'],
        $end_time,
        $data['conf_duration'],
        $data['conf_status']
    );
  }
  $output .= t('<h2>Finished conference</h2>');
  $output .= theme_table($header, $rows, $attributes);

  if (!empty($output)) {
    return $output;
  }
  else {
    return t('There is not history data!');
  }
}

/**
 * History record details display function
 *
 * @param $conf_num_id
 *   Numeric ID of the conference.
 *
 * @return
 *   Html table what is include the conference history data.
 */
function _mcu_booking_history_details ($conf_num_id) {
  global $_mcu_booking_web_user_id;

   db_set_active('iir2');
    $result = db_query("
      SELECT id, ember_id, conf_type, conf_name, conf_id, conf_num_id, conf_media,
             conf_entry_passwd, conf_chair_passwd, conf_video_session, conf_transfer_rate,
             conf_layout, conf_start_time, conf_duration, conf_status, conf_status_time, conf_comment
      FROM {mcu_reservation}
      WHERE ember_id = %d AND (conf_start_time < '%s' OR conf_status = 'deleted' AND conf_num_id = %d) ORDER BY conf_start_time DESC",  $_mcu_booking_web_user_id, date('Y-m-d H:i:s'), $conf_num_id);
  db_set_active();
 
}
