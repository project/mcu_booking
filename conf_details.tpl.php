<table id="mcu_booking_details" class="mcu_booking">
  <tr>
    <td><strong><?php print t('Conference short name:') ?></strong></td>
    <td><?php print $values['display_name'] ?></td>
  </tr>
  <tr>
    <td><strong><?php print t('Start time:') ?></strong></td>
    <td><?php print date('Y-m-d H:i:s', $values['start_time']) ?></td>
  </tr>
  <tr>
    <td><strong><?php print t('Duration:') ?></strong></td>
    <td><?php print check_plain($values['duration']) ?></td>
  </tr>
  <?php if ($values['DTMF']) { ?>
  <tr>
    <td><strong><?php print t('DTMF signals:') ?></strong></td>
    <td><?php print $values['DTMF'] ? t('true') : t('false') ?></td>
  </tr>
  <tr>
    <td><strong><?php print t('Chair password:') ?></strong></td>
    <td><?php print check_plain($values['chair_password']) ?></td>
  </tr>
  <tr>
    <td><strong><?php print t('Entry password:') ?></strong></td>
    <td><?php print check_plain($values['entry_password']) ?></td>
  </tr>
  <?php } ?>
  <tr>
    <td><strong><?php print t('Dial-in number:') ?></strong></td>
    <td><?php print check_plain($values['dial-in_number']) ?></td>
  </tr>
  <tr>
    <td><strong><?php print t('Layout:') ?></strong></td>
    <td><?php print $values['layout'] ?></td>
  </tr>
  <tr>
    <td><strong><?php print t('Transfer rate:') ?></strong></td>
    <td><?php print check_plain($values['transfer_rate']) ?></td>
  </tr>
  <tr>
    <td><strong><?php print t('Lecture mode:') ?></strong></td>
    <td><?php print check_plain($values['lecture_mode']) ? t('true') : t('false') ?></td>
  </tr>
  <!--permanetly removed, waiting polycom fix-->
  <!--tr>
    <td><strong><?php print t('Same layout:') ?></strong></td>
    <td><?php print check_plain($values['same_layout']) ? t('true') : t('false') ?></td>
  </tr-->
  <tr>
    <td><strong><?php print t('Encryption:') ?></strong></td>
    <td><?php print check_plain($values['encryption']) ? t('true') : t('false') ?></td>
  </tr>
</table>
