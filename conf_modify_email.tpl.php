<?php require('email_header.php'); ?>

    <p><?php print t('MCU resources has been modified according to your request. Please read conference information below.'); ?></p>

    <p><?php print t('Conference summary:'); ?><p>
    <ul>
      <li><?php print t('Conference short name: "!conf_short_name"', $reservation_data); ?></li>
      <li><?php print t('Reservation owner: !display_name', $reservation_data); ?></li>
      <!--li><?php print t('Username: !user_name', $reservation_data); ?></li-->
      <li><?php print t('Organization: !organization_name', $reservation_data); ?></li>
    </ul>
    <ul>
      <li><?php print t('Dial-in number: !dial_in_number (GDS)', $reservation_data); ?></li>
      <li><?php print t('Conference start time: !start_date_time (CET)', $reservation_data); ?></li>
      <li><?php print t('Duration: !duration hours', $reservation_data); ?></li>
      <li><?php print t('Conference type: !conf_type', $reservation_data); ?></li>
      <li><?php print t('Transfer rate: !transfare_rate Kbit/s', $reservation_data); ?></li>
      <li><?php print t('Layout: !layout', $reservation_data); ?></li>
      <li><?php print t('Advanced conference handling (IVR): !DTMF', $reservation_data); ?></li>
      <li><?php print t('Video encryption: !AES', $reservation_data); ?></li>
      <li><?php print t('Videoconference recording and streaming: !vcr', $reservation_data); ?></li>
    </ul>

<?php require('email_footer.php'); ?>
