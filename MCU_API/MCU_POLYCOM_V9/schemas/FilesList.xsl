<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="RESPONSE_TRANS_MCU/GET_DIRECTORY">
  <html>
  <head>
  <script language="javascript">

  function ShowUploadButton()
  {
    var UploadFields = document.forms.item(&quot;FileListForm&quot;).getElementsByTagName(&quot;input&quot;);

    for(i=0; i != UploadFields.length; i++)
    {
	if(UploadFields.item(i).type == &quot;file&quot;)
	{
	   document.forms.item(&quot;FileListForm&quot;).SubmitButton.style.display = &quot;block&quot;;
	   break;
	}
    }
  }

  </script>
  </head>
  <body>
  <center>
  <form name="FileListForm" action="upload.asp" enctype="multipart/form-data" method="post">
  <xsl:apply-templates select="DIRECTORY"/>
  <br/>
  <input type="submit" name="SubmitButton" value="Upload Files" style="display:none"/>
  </form>
  </center>
  <script language="javascript">
  ShowUploadButton();
  </script>

  </body>
  </html>
</xsl:template>

<xsl:template match="DIRECTORY">
    <xsl:variable name="DirName"> 
      <xsl:value-of select="NAME"/> 
    </xsl:variable>
    <xsl:variable name="DirWritable"> 
      <xsl:value-of select="WRITABLE"/> 
    </xsl:variable>
    <table border="1">
    <tr bgcolor="#9acd32">
	  <th align="middle">
	    <xsl:choose>
       	      <xsl:when test='$DirWritable=&quot;true&quot;'>
 	        <xsl:attribute name="colspan">
		  <xsl:text>3</xsl:text>
	        </xsl:attribute>
	      </xsl:when>
	      <xsl:otherwise>
 	        <xsl:attribute name="colspan">
		  <xsl:text>2</xsl:text>
	        </xsl:attribute>
	      </xsl:otherwise>
	    </xsl:choose>
	    <xsl:text>&quot;</xsl:text>      
	    <xsl:value-of select="$DirName"/>
	    <xsl:text>&quot; </xsl:text>
	    <xsl:text>directory files</xsl:text>
	  </th>
    </tr>
    <tr>
	  <th align="middle" colspan="2">File Name</th>
      	  <xsl:if test='$DirWritable=&quot;true&quot;'>
    	    <th align="middle">File to upload</th>
	  </xsl:if>
    </tr>
    <xsl:for-each select="FILE_NAME">
    <tr>
      <td style="padding-left:5;padding-right:5"><a href="{.}?download"><xsl:value-of select="."/></a></td>
      <td style="padding-left:5;padding-right:5">
	<button value="View" onclick="location.href = &apos;{.}&apos;">View</button>
      </td>
      <xsl:if test='$DirWritable=&quot;true&quot;'>
       <td style="padding-left:5;padding-right:5">
         <input type="file">
	   <xsl:attribute name="name">
	     <xsl:value-of select="$DirName"/>
	     <xsl:text>/</xsl:text>
	     <xsl:value-of select="."/>
	   </xsl:attribute>
         </input>
       </td>
      </xsl:if>
    </tr>
    </xsl:for-each>
    </table>
</xsl:template>

</xsl:stylesheet>