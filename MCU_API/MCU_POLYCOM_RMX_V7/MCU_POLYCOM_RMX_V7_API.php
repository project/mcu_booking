<?php
$module_path = drupal_get_path('module', 'mcu_booking');
require_once($module_path .'/MCU_API/MCU_COMMON_API.php');

/**
 * MCU API for Polycom RMX 6.x.x
 */
class MCU_POLYCOM_RMX_V7 extends MCU {
  private $mcu_token, $mcu_user_token;

  /**
   * Call the parent class construct and transfer them the initialisation variabels.
   *
   * Attempt to set tokens from the $_SESSION array, and call the login mechanism.
   *
   * @param $mcu_type
   *   Vendor of the MCU.
   * @param $mcu_version
   *   XML API version number of the MCU.
   * @param $mcu_user
   *   User how can access to the MCU.
   * @param $mcu_password
   *   The user's password.
   * @param $mcu_host
   *   Domain name or IP address of the MCU default is localhost.
   * @param $mcu_port
   *   Port of the MCU default is 80.
   */
  function __construct($mcu_type = NULL, $mcu_version =NULL , $mcu_user = NULL, $mcu_password = NULL, $mcu_host = NULL, $mcu_port = NULL) {
    parent::__construct($mcu_type, $mcu_version, $mcu_user, $mcu_password, $mcu_host, $mcu_port);

    $this->mcu_token    = $_SESSION['mcu_tokens']['mcu_token'];
    $this->mcu_user_token = $_SESSION['mcu_tokens']['mcu_user_token'];

    $this->mcu_login();
  }

  /**
   * MCU specific login mechanism.
   *
   * If there are tokens and they are valid return TRUE, else make a login.
   *
   * @return
   *   TRUE is success and FALSE if not.
   */
  protected function mcu_login() {
  //check to tokens are setted and valid
    if (empty($this->mcu_token) OR empty($this->mcu_user_token) OR !$this->is_token_valid()) {
      if ($this->load_xml('login')) {
        //set account informations in $this->xml
        $this->xml_set_element_content('/TRANS_MCU/ACTION/LOGIN/USER_NAME', $this->mcu_user);
        $this->xml_set_element_content('/TRANS_MCU/ACTION/LOGIN/PASSWORD', $this->mcu_password);
        $this->xml_set_element_content('/TRANS_MCU/ACTION/LOGIN/MCU_IP/IP', $this->mcu_host);
        //POST the xml to the MCU
        $this->xml_post();
        //Set tokens into the $_SESSION array for the future useage.
        $_SESSION['mcu_tokens']['mcu_token'] = $this->mcu_token = $this->xml_get_element_content('/RESPONSE_TRANS_MCU/ACTION/LOGIN/MCU_TOKEN');
        $_SESSION['mcu_tokens']['mcu_user_token'] = $this->mcu_user_token = $this->xml_get_element_content('/RESPONSE_TRANS_MCU/ACTION/LOGIN/MCU_USER_TOKEN');
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * MCU specific logout mechanism.
   *
   * @return
   *   TRUE if success and FALSE if not.
   */
  function mcu_logout() {
    if ($this->load_xml('logout')) {
      $this->set_tokens();
      if (!$this->xml_post()) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
    else {
      return FALSE;
    }
  }

  /**
   * MCU create conference reservation
   *
   * @param $res_data
   *   Reservation form data in array format.
   */
  public function mcu_reservation_create($res_data, $is_meeting_room = FALSE) {
    //set meeting room name parameters NIIFI specific
    //NEED WORK!!!
    if ($is_meeting_room) {
      $conf_name = $this->conf_name_generator(9000, 9998);
    }
    else {
      $conf_name = $this->conf_name_generator();
    }

    //load the vcr participant, NIIF SPECIFIC!!!
    if ($res_data['conf_archive_stream']) {
      $this->mcu_participant_add (array($res_data['ipvcr_name'], $res_data['ipvcr_e164']));
      $participant_xml = new DOMDocument;
      $participant_xml->loadXML($this->xml->saveXML());
    }

    $this->load_xml('reservation_create');
    $this->set_tokens();

    //set video streaming and recording, NIIF SPECIFIC!!!
    if ($res_data['conf_archive_stream']) {
      $party_root = $participant_xml->getElementsByTagName("PARTY")->item(0);
      $this->xml->getElementsByTagName('PARTY_LIST')->item(0)->appendChild($this->xml->importNode($party_root, TRUE));
    }

    //set attributes in $this->xml
    $duration = split(':', $res_data['conf_duration']);
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/START/RESERVATION/NAME', $conf_name);
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/START/RESERVATION/NUMERIC_ID', $conf_name);
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/START/RESERVATION/ID', -1);
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/START/RESERVATION/START_TIME', $this->mgc_time($res_data['conf_start_date']));
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/START/RESERVATION/DURATION/HOUR', strlen($duration[0]) < 2 ? '0'. $duration[0] : $duration[0]);
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/START/RESERVATION/DURATION/MINUTE', strlen($duration[1]) < 2 ? '0'. $duration[1] : $duration[1]);
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/START/RESERVATION/BILLING_DATA', $res_data['web_user_id']);
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/START/RESERVATION/CONTACT_INFO_LIST/CONTACT_INFO', $res_data['conf_name']);
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/START/RESERVATION/CONTACT_INFO_LIST/CONTACT_INFO2', $res_data['web_user_uname']);
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/START/RESERVATION/CONTACT_INFO_LIST/CONTACT_INFO3', $res_data['web_user_id']);
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/START/RESERVATION/CONTACT_INFO_LIST/CONTACT_INFO3', date('Y-m-d H:i:s'));
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/START/RESERVATION/DISPLAY_NAME', $this->unique_diplay_name($res_data['conf_name']));
  //  $this->xml_set_element_content('/TRANS_RES_1/ACTION/START/RESERVATION/HD', 'true');

    //set meeting room parameters
    if ($is_meeting_room) {
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/START/RESERVATION/MEETING_ROOM/ON', 'true');
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/START/RESERVATION/AUTO_TERMINATE/ON', 0);
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/START/RESERVATION/AUTO_TERMINATE/TIME_BEFORE_FIRST_JOIN', 1);
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/START/RESERVATION/AUTO_TERMINATE/TIME_AFTER_LAST_QUIT', 10);
    }
    else {
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/START/RESERVATION/MEETING_ROOM/ON', 'false');
    }

    //set the video stream encryption
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/START/RESERVATION/ENCRYPTION', $res_data['AES'] ? 'true' : 'false');

    //advanced layout
    if ($res_data['advanced_layout'] == 1) {
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/START/RESERVATION/SAME_LAYOUT', 'true');
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/START/RESERVATION/LECTURE_MODE/LECTURE_MODE_TYPE', 'lecture_none');
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/START/RESERVATION/CONTENT_TO_LEGACY_EPS', 'false');
    }
    elseif ($res_data['advanced_layout'] == 2) {
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/START/RESERVATION/SAME_LAYOUT', 'false');
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/START/RESERVATION/LECTURE_MODE/LECTURE_MODE_TYPE', 'lecture_mode');
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/START/RESERVATION/CONTENT_TO_LEGACY_EPS', 'false');
    }
    elseif ($res_data['advanced_layout'] == 3) {
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/START/RESERVATION/SAME_LAYOUT', 'false');
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/START/RESERVATION/LECTURE_MODE/LECTURE_MODE_TYPE', 'lecture_none');
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/START/RESERVATION/CONTENT_TO_LEGACY_EPS', 'true');
    }
    else {
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/START/RESERVATION/SAME_LAYOUT', 'false');
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/START/RESERVATION/LECTURE_MODE/LECTURE_MODE_TYPE', 'lecture_none');
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/START/RESERVATION/CONTENT_TO_LEGACY_EPS', 'false');
    }

    //DTMF alias IVR functionality settings
    if ($res_data['DTMF']) {
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/START/RESERVATION/AV_MSG', $this->ivr_name);
    }

    $this->xml_set_element_content('/TRANS_RES_1/ACTION/START/RESERVATION/VIDEO_QUALITY', 'motion');
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/START/RESERVATION/TRANSFER_RATE', $res_data['conf_transfer_rate']);
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/START/RESERVATION/LAYOUT', $res_data['conf_layout']);

    $this->xml_post();
    return $this->xml;
  }

  /**
   * Show the reserved conference list
   *
   * @return array
   *   Return an array with list of reserved conference if it exists and an empty array it is not exists any reserved conference
   */
  public function mcu_reservation_list() {
    $result = array();
    $this->load_xml('reservation_query_list');
    $this->set_tokens();
    $this->xml_post();

    $result_array = $this->xml_to_array($this->xml);
    $res_list = $result_array['RESPONSE_TRANS_RES_LIST']['ACTION']['GET_RES_LIST']['RES_SUMMARY_LS']['RES_SUMMARY'];

    if (is_array($res_list)) {
      if (!isset($res_list[0])) {
        $result[] = $this->api_map($res_list);
      }
      else {
        foreach ($res_list as $item) {
          $result[] = $this->api_map($item);
        }
      }
    }

    return $result;
  }

  /**
   * Show reserved conference details
   *
   * @param $conf_id
   *   ID of the conference
   *
   * @return array
   *   Return an array with details of conferences
   */
  public function mcu_reservation_get_details($conf_id) {
    $this->load_xml('reservation_get_details');
    $this->set_tokens();
    $this->xml_set_element_content('/TRANS_RES_2/ACTION/GET_RES/ID', $conf_id);
    $this->xml_post();
    $result = $this->xml_to_array($this->xml);
    return $this->api_map($result['RESPONSE_TRANS_RES']['ACTION']['GET_RES']['RESERVATION']);
  }

  /**
   * Update the reservations and meeting_rooms
   *
   * @param $res_data
   *   The resolution of the reservation or meeting room data
   * @param $conf_id
   *   The id (not the numeric_id) of the reservation or the meeting room
   * @param $is_meeting_room
   *   TRUE if it is and FALSE if it is not.
   *
   * @return object
   *   The entry response xml in DomDocument object format.
   */
  public function mcu_reservation_update($res_data, $conf_id, $is_meeting_room = FALSE) {
    //load the vcr participant, NIIF SPECIFIC!!!
    if ($res_data['conf_archive_stream']) {
      $this->mcu_participant_add (array($res_data['ipvcr_name'], $res_data['ipvcr_e164']));
      $participant_xml = new DOMDocument;
      $participant_xml->loadXML($this->xml->saveXML());
    }

    $this->load_xml('reservation_update');
    $this->set_tokens();

    //set video streaming and recording, NIIF SPECIFIC!!!
    if ($res_data['conf_archive_stream']) {
      $party_root = $participant_xml->getElementsByTagName("PARTY")->item(0);
      $this->xml->getElementsByTagName('PARTY_LIST')->item(0)->appendChild($this->xml->importNode($party_root, TRUE));
    }

    //set attributes in $this->xml
    $duration = split(':', $res_data['conf_duration']);
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/NAME', $res_data['name']);
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/ID', $conf_id);
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/START_TIME', $this->mgc_time($res_data['conf_start_date']));
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/DURATION/HOUR', strlen($duration[0]) < 2 ? '0'. $duration[0] : $duration[0]);
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/DURATION/MINUTE', strlen($duration[1]) < 2 ? '0'. $duration[1] : $duration[1]);
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/BILLING_DATA', $res_data['web_user_id']);
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/CONTACT_INFO_LIST/CONTACT_INFO', $res_data['conf_name']);
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/CONTACT_INFO_LIST/CONTACT_INFO2', $res_data['web_user_uname']);
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/CONTACT_INFO_LIST/CONTACT_INFO3', $res_data['web_user_id']);
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/CONTACT_INFO_LIST/CONTACT_INFO3', date('Y-m-d H:i:s'));
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/DISPLAY_NAME', $this->unique_diplay_name($res_data['conf_name']));

    //set meeting room parameters
    if ($is_meeting_room) {
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/MEETING_ROOM/ON', 'true');
    }
    else {
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/MEETING_ROOM/ON', 'false');
    }

    //set the video stream encryption
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/ENCRYPTION', $res_data['AES'] ? 'true' : 'false');


    //advanced layout
    if (!empty($res_data['advanced_layout']) AND $res_data['advanced_layout'] == 1) {
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/SAME_LAYOUT', 'true');
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/LECTURE_MODE/LECTURE_MODE_TYPE', 'lecture_none');
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/CONTENT_TO_LEGACY_EPS', 'false');
    }
    elseif (!empty($res_data['advanced_layout']) AND $res_data['advanced_layout'] == 2) {
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/SAME_LAYOUT', 'false');
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/LECTURE_MODE/LECTURE_MODE_TYPE', 'lecture_mode');
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/CONTENT_TO_LEGACY_EPS', 'false');
    }
    elseif (!empty($res_data['advanced_layout']) AND $res_data['advanced_layout'] == 3) {
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/SAME_LAYOUT', 'false');
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/LECTURE_MODE/LECTURE_MODE_TYPE', 'lecture_none');
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/CONTENT_TO_LEGACY_EPS', 'true');
    }
    else {
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/SAME_LAYOUT', 'false');
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/LECTURE_MODE/LECTURE_MODE_TYPE', 'lecture_none');
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/CONTENT_TO_LEGACY_EPS', 'false');
    }

    //DTMF alias IVR functionality settings
    if ($res_data['DTMF']) {
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/AV_MSG', $this->ivr_name);
    }

    $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/VIDEO_QUALITY', 'motion');
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/TRANSFER_RATE', $res_data['conf_transfer_rate']);
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/LAYOUT', $res_data['conf_layout']);

    $this->xml_post();
    return $this->xml;
  }

  public function mcu_reservation_delete($conf_id) {
    $this->load_xml('reservation_terminate');
    $this->set_tokens();
    $this->xml_set_element_content('/TRANS_RES_2/ACTION/TERMINATE_RES/ID', $conf_id);
    $this->xml_post();

    return $this->xml;
  }

  /**
   * Show ongoing conference list
   *
   * @return array
   *   Return an array with list of ongoing conferences if it exists and an empty array it is not exists any ongoing conference.
   */
  public function mcu_ongoing_list() {
    $result = array();
    $this->load_xml('ongoing_query_list');
    $this->set_tokens();
    $this->xml_post();

    $result_array = $this->xml_to_array($this->xml);
    $res_list = $result_array['RESPONSE_TRANS_CONF_LIST']['ACTION']['GET_LS']['CONF_SUMMARY_LS']['CONF_SUMMARY'];

    if (is_array($res_list)) {
      if (!isset($res_list[0])) {
        $result[] = $this->api_map($res_list);
      }
      else {
        foreach ($res_list as $item) {
          $result[] = $this->api_map($item);
        }
      }
    }
    return $result;
  }

  public function mcu_ongoing_get_details($conf_id) {
    $this->load_xml('ongoing_get_details');
    $this->set_tokens();
    $this->xml_set_element_content('/TRANS_CONF_2/ACTION/GET/ID', $conf_id);
    $this->xml_post();

    $result = $this->xml_to_array($this->xml);
    if (is_array($result['RESPONSE_TRANS_CONF']['ACTION']['GET']['CONFERENCE']['RESERVATION']) AND is_array($result['RESPONSE_TRANS_CONF']['ACTION']['GET']['CONFERENCE']['ONGOING_PARTY_LIST'])) {
      $result = array_merge($result['RESPONSE_TRANS_CONF']['ACTION']['GET']['CONFERENCE']['RESERVATION'], $result['RESPONSE_TRANS_CONF']['ACTION']['GET']['CONFERENCE']['ONGOING_PARTY_LIST']);
    }
    else {
      $result = array();
    }
    return $this->api_map($result);
  }

  public function mcu_ongoing_update($conf_id) {
  }

  public function mcu_ongoing_update_endtime($conf_id, $end_time) {
    $this->load_xml('ongoing_update_endtime');
    $this->set_tokens();
    $this->xml_set_element_content('/TRANS_CONF_2/ACTION/SET_END_TIME/ID', $conf_id);
    $this->xml_set_element_content('/TRANS_CONF_2/ACTION/SET_END_TIME/END_TIME', $this->mgc_time($end_time));
    $this->xml_post();
  }

  public function mcu_ongoing_terminate($conf_id) {
    $this->load_xml('ongoing_terminate');
    $this->set_tokens();
    $this->xml_set_element_content('/TRANS_CONF_2/ACTION/TERMINATE_CONF/ID', $conf_id);
    $this->xml_post();

    sleep(4);
    $msg = $this->xml_get_element_content('//RETURN_STATUS/DESCRIPTION');
    if ($msg  === "OK ") {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  public function mcu_ongoing_mute($conf_id) {return;}
  public function mcu_ongoing_lock($conf_id) {return;}
  public function mcu_ongoing_set_volume($conf_id, $volume) {return;}

  public function mcu_meeting_room_list() {
    $result = array();
    $this->load_xml('meeting_room_query_list');
    $this->set_tokens();
    $this->xml_post();

    $result_array = $this->xml_to_array($this->xml);
    $res_list = $result_array['RESPONSE_TRANS_RES_LIST']['ACTION']['GET_MEETING_ROOM_LIST']['MEETING_ROOM_SUMMARY_LS']['MEETING_ROOM_SUMMARY'];

    if (is_array($res_list)) {
      if (!isset($res_list[0])) {
        $result[] = $this->api_map($res_list);
      }
      else {
        foreach ($res_list as $item) {
          $result[] = $this->api_map($item);
        }
      }
    }

    return $result;

  }

  public function mcu_meeting_room_get_details($conf_id) {
    $this->load_xml('meeting_room_get_details');
    $this->set_tokens();
    $this->xml_set_element_content('/TRANS_RES_2/ACTION/GET_MEETING_ROOM/ID', $conf_id);
    $this->xml_post();

    $result = $this->xml_to_array($this->xml);
    return $this->api_map($result['RESPONSE_TRANS_RES']['ACTION']['GET_MEETING_ROOM']['RESERVATION']);
  }

  public function mcu_meeting_room_update($res_data, $conf_id) {
    //load the vcr participant, NIIF SPECIFIC!!!
    if ($res_data['conf_archive_stream']) {
      $this->mcu_participant_add (array($res_data['ipvcr_name'], $res_data['ipvcr_e164']));
      $participant_xml = new DOMDocument;
      $participant_xml->loadXML($this->xml->saveXML());
    }

    $this->load_xml('reservation_update');
    $this->set_tokens();

    //set video streaming and recording, NIIF SPECIFIC!!!
    if ($res_data['conf_archive_stream']) {
      $party_root = $participant_xml->getElementsByTagName("PARTY")->item(0);
      $this->xml->getElementsByTagName('PARTY_LIST')->item(0)->appendChild($this->xml->importNode($party_root, TRUE));
    }

    //set attributes in $this->xml
    $duration = split(':', $res_data['conf_duration']);
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/NAME', $res_data['name']);
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/ID', $conf_id);
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/START_TIME', $this->mgc_time($res_data['conf_start_date']));
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/DURATION/HOUR', strlen($duration[0]) < 2 ? '0'. $duration[0] : $duration[0]);
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/DURATION/MINUTE', strlen($duration[1]) < 2 ? '0'. $duration[1] : $duration[1]);
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/BILLING_DATA', $res_data['web_user_id']);
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/CONTACT_INFO_LIST/CONTACT_INFO', 'NIIF Booking System: '. $res_data['conf_name'] .' reservation by '. $res_data['web_user_uname'] .' (uid='. $res_data['web_user_id'] .') at '. date('Y-m-d H:i:s'));
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/DISPLAY_NAME', $this->unique_diplay_name($res_data['conf_name']));

    //set meeting room parameters
    if ($is_meeting_room) {
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/MEETING_ROOM/ON', 'true');
    }
    else {
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/MEETING_ROOM/ON', 'false');
    }

    //set the video stream encryption
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/ENCRYPTION', $res_data['AES'] ? 'true' : 'false');

    //advanced layout
    if (!empty($res_data['advanced_layout']) AND $res_data['advanced_layout'] == 1) {
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/SAME_LAYOUT', 'true');
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/LECTURE_MODE/LECTURE_MODE_TYPE', 'lecture_none');
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/CONTENT_TO_LEGACY_EPS', 'false');
    }
    elseif (!empty($res_data['advanced_layout']) AND $res_data['advanced_layout'] == 2) {
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/SAME_LAYOUT', 'false');
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/LECTURE_MODE/LECTURE_MODE_TYPE', 'lecture_mode');
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/CONTENT_TO_LEGACY_EPS', 'false');
    }
    elseif (!empty($res_data['advanced_layout']) AND $res_data['advanced_layout'] == 3) {
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/SAME_LAYOUT', 'false');
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/LECTURE_MODE/LECTURE_MODE_TYPE', 'lecture_none');
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/CONTENT_TO_LEGACY_EPS', 'true');
    }
    else {
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/SAME_LAYOUT', 'false');
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/LECTURE_MODE/LECTURE_MODE_TYPE', 'lecture_none');
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/CONTENT_TO_LEGACY_EPS', 'false');
    }

    //DTMF alias IVR functionality settings
    if ($res_data['DTMF']) {
      $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/AV_MSG', $this->ivr_name);
    }

    $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/VIDEO_QUALITY', 'motion');
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/TRANSFER_RATE', $res_data['conf_transfer_rate']);
    $this->xml_set_element_content('/TRANS_RES_1/ACTION/UPDATE/RESERVATION/LAYOUT', $res_data['conf_layout']);

    $this->xml_post();
    return $this->xml;
  }

  public function mcu_meeting_room_terminate($conf_id) {
    $this->load_xml('meeting_room_terminate');
    $this->set_tokens();
    $this->xml_set_element_content('/TRANS_RES_2/ACTION/TERMINATE_MEETING_ROOM/ID', $conf_id);
    $this->xml_post();
    $this->xml_post();

    return $this->xml;
  }

  public function mcu_participant_add($data, $conf_id = NULL) {
    $this->load_xml('participant');

    $this->xml_set_element_content('/PARTY/NAME', $data[0]);
    $this->xml_set_element_content('/PARTY/ALIAS/NAME', $data[1]);
  }

  public function mcu_paticipant_kick($participant) {return;}
  public function mcu_participant_delete($participant) {return;}

  public function mcu_set_video_layout($conf_id, $layout) {
    $this->load_xml('ongoing_update_layout');
    $this->set_tokens();

    $this->xml_set_element_content('/TRANS_CONF_1/ACTION/SET_VIDEO_LAYOUT/ID', $conf_id);
    $this->xml_set_element_content('/TRANS_CONF_1/ACTION/SET_VIDEO_LAYOUT/FORCE/LAYOUT', $layout);
    $this->xml_post();
  }

  public function mcu_set_DTMF($conf_id) {return;}

  /**
   * Set mcu and user tokens into $this->xml variable.
   */
  protected function set_tokens() {
    $this->xml_set_element_content('//MCU_TOKEN', $this->mcu_token);
    $this->xml_set_element_content('//MCU_USER_TOKEN', $this->mcu_user_token);
  }

  /**
   * Validate the mcu and user tokens
   *
   * @return
   *   TRUE if success and FALSE if not
   */
  protected function is_token_valid() {
    if ($this->load_xml('get_state')) {
      $this->set_tokens();
      if ($this->xml_post(TRUE)) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Create an array from an xml multi values tag.
   *
   * @param $xml
   *   The part of the xml what is a list.
   *
   * @return
   *   An array if success and an empty array if not.
   */
  protected function xml_to_array($root) {
    $result = array();

    if ($root->hasAttributes()) {
      $attrs = $root->attributes;

      foreach ($attrs as $i => $attr) {
        $result[$attr->name] = $attr->value;
      }
    }

    $children = $root->childNodes;

    if ($children->length == 1) {
      $child = $children->item(0);

      if ($child->nodeType == XML_TEXT_NODE) {
        $result['_value'] = $child->nodeValue;

        if (count($result) == 1) {
          return $result['_value'];
        }
        else {
          return $result;
        }
      }
    }

    $group = array();

    for ($i = 0; $i < $children->length; $i++) {
      $child = $children->item($i);

      if (!isset($result[$child->nodeName])) {
        $result[$child->nodeName] = $this->xml_to_array($child);
      }
      else {
        if (!isset($group[$child->nodeName])) {
          $tmp = $result[$child->nodeName];
          $result[$child->nodeName] = array($tmp);
          $group[$child->nodeName] = 1;
        }

        $result[$child->nodeName][] = $this->xml_to_array($child);
      }
    }

    return $result;
  }

  private function api_map($in) {
    $prefix = $in['DIAL_IN_H323_SRV_PREFIX_LIST']['DIAL_IN_H323_SRV_PREFIX']['PREFIX'];
    //mcu software version 7 wrong duration time workaround add +1 minute
    $minute = $in['DURATION']['MINUTE']+1;
    empty($prefix) ? $prefix = variable_get('mcu_booking_routing_prefix', NULL) : NULL;
    return array(
      'chair_password'   => !empty($in['PASSWORD']) ? $in['PASSWORD'] : '',
      'display_name'     => $this->unique_diplay_name($in['DISPLAY_NAME'], TRUE),
      'duration'         => $in['DURATION']['HOUR'] .':'. sprintf('%02d', $in['DURATION']['MINUTE']),
      'dial-in_number'   => $prefix . $in['NAME'],
      'DTMF'             => !empty($in['AV_MSG']) ? TRUE : FALSE,
      'encryption'       => $in['ENCRYPTION'] == 'true' ? TRUE : FALSE,
      'end_time'         => strtotime('-'. $in['DURATION']['HOUR'] .':'. $minute .' hour', strtotime($in['START_TIME'])),
      'entry_password'   => !empty($in['ENTRY_PASSWORD']) ? $in['ENTRY_PASSWORD'] : '',
      'h323_name'        => $in['DIAL_IN_H323_SRV_PREFIX_LIST']['DIAL_IN_H323_SRV_PREFIX']['NAME'],
      'h323_prefix'      => $in['DIAL_IN_H323_SRV_PREFIX_LIST']['DIAL_IN_H323_SRV_PREFIX']['PREFIX'],
      'id'               => $in['ID'],
      'layout'           => $in['LAYOUT'] ? $in['LAYOUT'] : '1x1',
      'lecture_mode'     => $in['LECTURE_MODE']['LECTURE_MODE_TYPE'] === 'lecture_mode' ? 'true' : 'false' ,
      'legacy'           => $in['CONTENT_TO_LEGACY_EPS'],
      'name'             => $in['NAME'],
      'min_num_of_participant' => $in['MEET_ME_PER_CONF']['MIN_NUM_OF_PARTIES'],
      'numeric_id'       => $in['NUMERIC_ID'],
      'same_layout'      => $in['SAME_LAYOUT'],
      'status'           => $in['RES_STATUS'],
      'start_time'       => strtotime('+00:00 hours', strtotime($in['START_TIME'])),
      'transfer_rate'    => $in['TRANSFER_RATE'],
      'vcr'              => empty($in['PARTY_LIST']) ? FALSE : TRUE,
      'video_session'    => $in['VIDEO_SESSION'],
      'web_reserved_uid' => $in['BILLING_DATA'],
      'participants'     => $in['ONGOING_PARTY']['PARTY_CONF_FORCE'],
    );
  }

  private function child_nodes($nodes, $parent_tag = NULL, $accu = array()) {
    static $accu;
    foreach ($nodes as $node) {
        if ($this->has_child($node)) {
          $this->child_nodes($node->childNodes, $node->nodeName .'_', $accu);
        }
        elseif ($node->nodeType == XML_ELEMENT_NODE) {
          $accu[$parent_tag . $node->nodeName] = $node->nodeValue;
        }
      }
    return $accu;
  }

  private function has_child($element) {
    if ($element->hasChildNodes()) {
      foreach ($element->childNodes as $c) {
        if ($c->nodeType == XML_ELEMENT_NODE) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  private function conf_name_generator($min=1, $max=8999) {
    $reservations  = $this->mcu_reservation_list();
    $ongoings      = $this->mcu_ongoing_list();
    $meeting_rooms = $this->mcu_meeting_room_list();

    $confs = array_merge($reservations, $ongoings, $meeting_rooms);
    $equal = FALSE;

    do {
      $conference_name = sprintf('%04d', rand($min, $max));
      if (!empty($confs) AND is_array($confs)) {
        foreach ($confs as $conf) {
          if ($conf['name'] == $conference_name) {
            $equal = TRUE;
          }
        }
      }
    } while ($equal);

    return $conference_name;
  }

  private function mgc_time($date_time) {
    return gmdate('Y-m-d\TH:i:s', strtotime($date_time . date('O')));
  }

  private function unique_diplay_name($displayname, $decode=FALSE) {
    if ($this->display_name_workaround) {
      if ($decode) {
        return preg_replace('/^[^_]+_/', '', $displayname, 1);
      }
      else {
        return time() . mt_rand(10,99) .'_'. $displayname;
      }
    }
    return $displayname;
  }
}
