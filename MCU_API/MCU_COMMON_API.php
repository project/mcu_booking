<?php
/**
 * Abstract MCU class
 *
 * Implemented the general xml utility and the API skeleton.
 */
abstract class MCU {
  protected $mcu_type, $mcu_version, $xml, $mcu_host, $mcu_port, $mcu_user, $mcu_password;
  protected $mcu_path, $api_path, $mgc_audioconf_type, $ivr_type, $ivr_name, $debug;
  public $curl, $debug_msg;

  function __construct($mcu_type = NULL, $mcu_version = NULL, $mcu_user = NULL,  $mcu_password = NULL, $mcu_host = NULL, $mcu_port = NULL) {
    $errors = array();
    $debug_msg = NULL;
    $this->mcu_config_init();

    !empty($mcu_type)?$this->mcu_type = $mcu_type:NULL;
    !empty($mcu_host)?$this->mcu_host = $mcu_host:NULL;
    !empty($mcu_port)?$this->mcu_port = $mcu_port:NULL;
    !empty($mcu_user)?$this->mcu_user = $mcu_user:NULL;
    !empty($mcu_version)?$this->mcu_version   = $mcu_version:NULL;
    !empty($mcu_password)?$this->mcu_password = $mcu_password:NULL;
    !empty($mcu_protocol)?$this->mcu_protocol = $mcu_protocol:NULL;

    $this->xml          = new DOMDocument('1.0', 'UTF-8');
    $this->api_path     = $this->mcu_path .'/MCU_API/MCU_'. $this->mcu_type .'_'. $this->mcu_version;

    //intialise the tcp connection
    $this->curl = curl_init();
    $host = $this->mcu_protocol .'://'. $this->mcu_host;
    curl_setopt($this->curl, CURLOPT_URL, $host);  //Set the MCU url
    curl_setopt($this->curl, CURLOPT_PORT, $this->mcu_port); //Set the MCU port
    curl_setopt($this->curl, CURLOPT_HTTPHEADER, array('Content-Type: text/xml')); //Set the posting data mime type
    curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, TRUE); //Return the contents of the call
    curl_setopt($this->curl, CURLOPT_FORBID_REUSE, FALSE);  //Reuse the live tcp connection
    curl_setopt($this->curl, CURLOPT_FRESH_CONNECT, FALSE); //Do not create a new tcp connection
    curl_setopt($this->curl, CURLOPT_TIMEOUT, 30);          //Connection falied timeout
    curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, FALSE);//DISABLED CERTIFICATION VERIFY
    curl_setopt($this->curl, CURLOPT_SSL_VERIFYHOST, FALSE);//DISABLED HOST VERIFY
    curl_setopt($this->curl, CURLOPT_POST, TRUE);           //POST the request
  }

  protected function mcu_config_init() {
    $conf = parse_ini_file('config.cfg');

    if (!empty($conf)) {
      foreach ($conf as $key => $value) {
        $this->$key = $value;
      }
    }
  }

  /**
   * Post the request xml whit curl from $this->xml and store the result xml in $this->xml.
   *
   * @return
   *   NULL string if success and error message if not
   */
  protected function xml_post($token_check = FALSE) {
    if (!empty($this->xml) AND $this->xml->schemaValidate($this->api_path .'/schemas/xsd/'. strtolower($this->xml->getElementsByTagName('*')->item(0)->tagName) .'.xsd')) {
      $this->xml_set_element_content('/TRANS_MCU/ACTION/LOGIN/MCU_IP/LISTEN_PORT', $this->mcu_port);
      $output = $this->xml->saveXML($this->xml->documentElement);
      curl_setopt($this->curl, CURLOPT_POSTFIELDS, $output);  //POSTed content

      //debug information
      $this->set_debug($this->print_xml($output));

      $result = curl_exec($this->curl);
      if (!empty($result)) {
        $this->xml->loadXML($result);
        $error = $this->xml_get_element_content('DESCRIPTION');

        //debug information
        $this->set_debug($this->print_xml());

        if ($error !== 'OK ') {
          $this->set_error($error);
          return FALSE;
        }
        else {
          return TRUE;
        }
      }
      else {
        $this->set_error('CURL ERROR: '. curl_error($this->curl));
        return FALSE;
      }
    }
    else {
      $this->set_error('Not valid xml!<br /><pre>!xml</pre>', array('!xml' => $this->print_xml()));
      return FALSE;
    }
  }

  /**
   * Create an DomDocument object from an xml file.
   *
   * You can call this function by entering name of the xml without the file extension.
   *
   * @param $xml_name
   *   "$xml_name" equal the file name in the current dictionary.
   */
  public function load_xml($xml_name) {
    return $this->xml->load($this->api_path .'/schemas/'. $xml_name .'.xml');
  }

  /**
   * Given back $this->xml in human readable formated HTML.
   *
   * @return
   *   Return the xml in HTML if success and FALSE if not.
   */
  public function print_xml($xml = NULL) {
    require_once(drupal_get_path('module', 'mcu_booking') .'/beautyXML/BeautyXML.class.php');
    $xml_formatter = new BeautyXML();

    if (!empty($xml)) {
      $printable_xml = '<pre>'. htmlentities($xml_formatter->format($xml), ENT_QUOTES) .'</pre>';
      return $printable_xml;
    }
    elseif (!empty($this->xml) AND $formatted_output = $this->xml->saveXML()) {
      $printable_xml = '<pre>'. htmlentities($xml_formatter->format($formatted_output), ENT_QUOTES) .'</pre>';
      return $printable_xml;
    }
  }

  /**
   * Get $element in $this->xml dom document return with the text node $content
   */
  function xml_get_element_content($tag) {
    $xpath = new DOMXPath($this->xml);
    $elements = $xpath->query($tag);
    $element = $elements->item(0);

    if (!empty($element)) {
      if ($element->hasChildNodes()) {
        $content = $element->textContent;
        return $content;
      }
      else {
        return FALSE;
      }
    }
    else {
      return FALSE;
    }
  }

  /**
   * Set $element content in $this->xml dom document
   */
  function xml_set_element_content($tag, $content) {
    $xpath = new DOMXPath($this->xml);
    $elements = $xpath->query($tag);

    if ($elements->length > 0 AND is_string($content)) {
      $element = $elements->item(0);

      if ($element->hasChildNodes()) {
        $element->replaceChild(new DomText($content), $element->firstChild);
      }
      else {
        $element->appendChild(new DomText($content));
      }
    }
    else {
      return FALSE;
    }
  }

  /**
   * Error setter. Cautch the errror messages and store them in an array.
   *
   * @param $error_message
   *   Error message.
   */
  protected function set_error($error_msg) {
    if (!($error_msg !== 'STATUS_FAIL_TO_LOCATE_USER ' OR $error_msg !== 'IN_PROGRESS ')) {
      $this->errors[] = $error_msg;
    }
  }

  /**
   * Error getter. Give back the error messages in an array.
   *
   * @return $errors
   *   Error messages in an array
   */
  public function get_error() {
    return $this->errors;
  }

  function set_debug($message) {
    if ($this->debug) {
      $this->debug_msg .= $message;
    }
  }

  protected function mcu_config_set() {}
  protected function mcu_config_get() {}

  abstract protected function mcu_login();
  abstract function mcu_logout();
  abstract function mcu_reservation_create($res_data);
  abstract function mcu_reservation_list();
  abstract function mcu_reservation_get_details($conf_id);
  abstract function mcu_reservation_update($res_data, $conf_id);
  abstract function mcu_reservation_delete($conf_id);
  abstract function mcu_ongoing_list();
  abstract function mcu_ongoing_get_details($conf_id);
  abstract function mcu_ongoing_update($conf_id);
  abstract function mcu_ongoing_terminate($conf_id);
  abstract function mcu_ongoing_mute($conf_id);
  abstract function mcu_ongoing_lock($conf_id);
  abstract function mcu_ongoing_set_volume($conf_id, $volume);
  abstract function mcu_participant_add($data, $conf_id = NULL);
  abstract function mcu_paticipant_kick($participant);
  abstract function mcu_participant_delete($participant);
  abstract function mcu_set_video_layout($conf_id, $layout);
  abstract function mcu_set_DTMF($conf_id);
}
