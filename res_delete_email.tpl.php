<?php require('email_header.php'); ?>

    <p><?php print t('MCU resources has been deleted according to your request. Please read reservation information below.'); ?></p>

    <p><?php print t('Conference summary:'); ?><p>
    <ul>
      <li><?php print t('Conference short name: "!conf_short_name"', $reservation_data); ?></li>
      <li><?php print t('Reservation owner: !display_name', $reservation_data); ?></li>
      <!--li><?php print t('Username: !user_name', $reservation_data); ?></li-->
      <li><?php print t('Organization: !organization_name', $reservation_data); ?></li>
    </ul>

<?php require('email_footer.php'); ?>
