<?php
// $Id$

/**
 * @file
 *   Ongoin conference parts of the mcu booking UI
 */

function _mcu_ongoing_delete_form($form_state = NULL) {
  global $_mcu_booking_mcu;
  $conf_id = arg(1);
  $form = array();

  $res_data = $_mcu_booking_mcu->mcu_ongoing_get_details($conf_id);

  $form['display_conf_name'] = array(
    '#type'   => 'markup',
    '#value'  => t("Do you really want to delete \"%conf_name\" conference", array('%conf_name' => $res_data['display_name'])),
    '#prefix' => '<div id="conf_name">',
    '#suffix' => '</div>',
  );

  $form['conf_name']  = array('#type' => 'value', '#value' => $res_data['display_name']);
  $form['conf_id']    = array('#type' => 'value', '#value' => $conf_id);
  $form['cancel']     = array('#type' => 'submit', '#value' =>  t('Cancel'));
  $form['mcu_delete'] = array('#type' => 'submit', '#value' => t('Delete'));

  return $form;
}

function _mcu_ongoing_delete_form_submit($form, &$form_state) {
  global $_mcu_booking_mcu;
  $v = $form_state['values'];
  $cid = $v['conf_id'];

  if ($form_state['values']['op'] !== t('Cancel')) {
    if ($_mcu_booking_mcu->mcu_ongoing_terminate($cid)) {
      drupal_set_message(t("Conference with %conf_short_name name terminated.", array('%conf_short_name' => $v['conf_name'])));
      watchdog('mcu_booking', '#@cid %conf_short_name conference was terminated.', array('@cid' => $cid, '%conf_short_name' => $v['conf_name']), 'info');
      drupal_goto('mcu_booking');
    }
    else {
      watchdog('mcu_booking', '#@cid conference couldn\'t terminated.', array('@cid' => $cid), 'error');
      drupal_goto('mcu_booking');
    }
  }
  else {
    drupal_goto('mcu_booking');
  }

}

function _mcu_ongoing_get_details($conf_id) {
  global $_mcu_booking_mcu, $_mcu_booking_module_path;

  $details = $_mcu_booking_mcu->mcu_ongoing_get_details($conf_id);

  if(!empty($details['id'])){
    $details['layout'] = theme_image($_mcu_booking_module_path .'/images/'. check_plain($details['layout']) .'.gif', check_plain($details['layout']));
    return theme('conf_details', $details);
  }
  else {
    drupal_set_message(t('Non-existent conference with this ID: !id', array('!id' => $conf_id)), 'error');
    drupal_goto('mcu_booking');
  }
}

function _mcu_ongoing_modify($form_state = NULL) {
  global $_mcu_booking_mcu, $_mcu_booking_module_path;
  $conf_id = arg(1);
  $res_data = $_mcu_booking_mcu->mcu_ongoing_get_details($conf_id);

  $tmp = _mcu_booking_common_form();
  $form['conf_layout'] = $tmp['conf_layout'];
  unset($tmp);

  $form['conf_layout']['#default_value'] = $res_data['layout'];

  $form['conf_end_date_time'] = array(
    '#type' => 'date_select',
    '#title' => t('Conference end time'),
    '#size' => 11,
    '#date_type' => DATE_DATETIME,
    '#date_timezone' => date_default_timezone_name(),
    '#date_format' => 'Y-m-d - H:i',
    '#date_increment' => 1,
    '#date_year_range' => '0:+1',
    '#default_value' => date('Y-m-d H:i:s', $res_data['end_time']),
    '#weight' => -44,
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Modify'),
    '#weight' => -24,
  );

  return $form;
}

//TODO
function _mcu_ongoing_modify_validate($form, &$form_state) {}

function _mcu_ongoing_modify_submit($form, &$form_state) {
  global $_mcu_booking_mcu, $_mcu_booking_web_user_id, $_mcu_booking_web_user_name;
  $conf_id = arg(1);

  //create and clean the data array
  $v = $form_state['values'];

  $_mcu_booking_mcu->mcu_set_video_layout($conf_id, $v['conf_layout']);
  $_mcu_booking_mcu->mcu_ongoing_update_endtime($conf_id, $v['conf_end_date_time']);

  drupal_set_message(t('Modify conference was succesfull.'));
}
