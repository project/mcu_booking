<?php require('email_header.php'); ?>

    <p><?php print t('MCU resources has been allocated according to your request. Please read meeting room information below.'); ?></p>

    <p><?php print t('Meeting room summary:'); ?><p>
    <ul>
      <li><?php print t('Meeting room short name: "!conf_short_name"', $reservation_data); ?></li>
      <li><?php print t('Reservation owner: !display_name', $reservation_data); ?></li>
      <!--li><?php print t('Username: !user_name', $reservation_data); ?></li-->
      <li><?php print t('Organization: !organization_name', $reservation_data); ?></li>
    </ul>
    <ul>
      <li><?php print t('Dial-in number: !dial_in_number (GDS)', $reservation_data); ?></li>
      <li><?php print t('Meeting room type: !conf_type', $reservation_data); ?></li>
      <li><?php print t('Transfer rate: !transfare_rate Kbit/s', $reservation_data); ?></li>
      <li><?php print t('Layout: !layout', $reservation_data); ?></li>
      <li><?php print t('Advanced conference handling (IVR): !DTMF', $reservation_data); ?></li>
      <li><?php print t('Video encryption: !AES', $reservation_data); ?></li>
    </ul>

    <ul>
      <?php print t('To enter the meeting room:'); ?>
      <li><?php print t('Using your H.323 videoconference endpoint, please call the dial-in number given above.'); ?></li>
      <li><?php print t('Using your phone (cell/PSTN or HBONE VoIP phone), please call number +36 1 666 1111, in the menu enter the full length dial-in number provided above using numeric keypad of your telephone device.'); ?></li>
    </ul>

<?php require('email_footer.php'); ?>
