<?php require('email_header.php'); ?>

    <p><?php print t('MCU resources has been terminated according to your request. Please read meeting room information below.'); ?></p>

    <p><?php print t('Meeting room summary:'); ?><p>
    <ul>
      <li><?php print t('Meeting room short name: "!conf_short_name"', $reservation_data); ?></li>
      <li><?php print t('Reservation owner: !display_name', $reservation_data); ?></li>
      <!--li><?php print t('Username: !user_name', $reservation_data); ?></li-->
      <li><?php print t('Organization: !organization_name', $reservation_data); ?></li>
    </ul>

<?php require('email_footer.php'); ?>
