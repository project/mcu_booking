<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="hu" lang="hu" dir="ltr">
  <?php
    $_SERVER['HTTPS'] === 'ON' ? $url = 'https://' : $url = 'http://';
    $url .= $_SERVER['SERVER_NAME'] . base_path() . drupal_get_path('module', 'mcu_booking');
  ?>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style type="text/css">
      body {
        width: 80%;
        margin: auto;
        font: 12pt arial;
      }

      a {
        color: rgb(13, 124, 140);
      }

      #header {
        background: transparent url('<?php print $url .'/design/mcu_header_bg_1px.jpg' ?>') repeat-x top left;
        position: relative;
        overflow: hidden;
      }

      #headline {
        font: bold 16pt "Times New Roman";
        color: rgb(46,98,143);
        text-align: right;
        position: absolute;
        right: 0;
        top: 0;
        height: 34pt;
        overflow: hidden;
        margin-left: 120px;
        z-index: 1000;
      }

      #salutation {
        font: bold 14pt "Times New Roman";
        background: transparent url('<?php print $url .'/design/mcu_headline_bg.jpg' ?>') no-repeat top center;
        text-align: center;
        color: #fff;
      }

      #footer {
        font: 11pt arial;
        background: transparent url('<?php print $url .'/design/mcu_footer_bg_1px.jpg' ?>') repeat-x top left;
        text-align: center;
        height: 50px;
      }

      ul { list-style-type:none }
    </style>
  </head>
  <body>
    <div id="header"><span><img src="<?php print $url .'/design/mcu_email_logo.jpg'?>"></span><span id="headline"><?php print t('NIIF Video Conference Booking System') ?></span></div>
    <h2 id="salutation"><?php print t('Dear !display_name,', $reservation_data);?></h2>
