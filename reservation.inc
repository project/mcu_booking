<?php
// $Id$

/**
 * @file
 *   Reservation part of the mcu booking UI.
 */

function _mcu_reservation_form($form_state = NULL) {
  return _mcu_booking_common_form();
}

//NEED WORKS!!!
function _mcu_reservation_form_validate($form, &$form_state) {
  if (strtotime($form_state['values']['conf_start_date']) <= time()) {
      form_set_error('conf_start_date', t('Its not possible to book confernce in the past.'));
  }
}

function _mcu_reservation_form_submit($form, &$form_state) {
  global $_mcu_booking_mcu, $_mcu_booking_web_user_uname, $_mcu_booking_web_user_id;

  //create and clean the data array
  $v = $form_state['values'];

  //set default values
  $v['web_user_id']    = $_mcu_booking_web_user_id;
  $v['web_user_uname']    = $_mcu_booking_web_user_uname;
  $v['conf_type'] === 'voice_conference'?$v['conf_media'] = 'audio':$v['conf_media'] = 'video_audio';
  empty($v['conf_layout']) ? $v['conf_layout']='1x1' : NULL;
  if ($v['conf_archive_stream']) {
    $v['ipvcr_name'] = variable_get('mcu_booking_ipvcr_name', NULL);
    $v['ipvcr_e164'] = variable_get('mcu_booking_ipvcr_e164', NULL);
  }

  //create the reservation on the MCU
  $result = $_mcu_booking_mcu->mcu_reservation_create($v);

  //Check if the reservation was successful
  $is_success = $result->getELementsByTagName('ID')->item(0)->textContent;
  if (is_string($is_success) && $is_success === '0') {
    $v['prefix']             = variable_get('mcu_booking_routing_prefix', NULL);
    $v['conf_number']       = $result->getElementsByTagName('NAME')->item(0)->textContent;
    $v['conf_id']           = $result->getElementsByTagName('ID')->item(1)->textContent;
    $v['conf_num_id']       = $result->getElementsByTagName('NUMERIC_ID')->item(0)->textContent;
    $v['conf_entry_pass']   = $result->getElementsByTagName('ENTRY_PASSWORD')->item(0)->textContent;
    $v['conf_chair_pass']   = $result->getElementsByTagName('LEADER_PASSWORD')->item(0)->textContent;

    //log into the db the success reservation data
    _res_data_log_db($v);

    //send an email to the user and the administrator
    _send_mail($v);

    watchdog('mcu_booking', '#!cid !conf_short_name conference was created.', array('!cid' => $v['conf_num_id'], '!conf_short_name' => $v['conf_name']), WATCHDOG_INFO);
    //redirect the browser to the reserved conference detail page
    drupal_goto('reservation_details/'. check_plain($v['conf_id']) .'/view');
  }
  else {
    $error = _error_handling(&$result);
    form_set_error($error['element'], $error['error'], FALSE);
    $form_state['rebuild'] = TRUE;
  }
}

function _mcu_reservation_delete_form($form_state = NULL) {
 // global $_mcu_booking_mcu;
  $form_tmp = _mcu_reservation_modify();

  $form['delete_question'] = array(
    '#type'   => 'item',
    '#value'  => t("Do you really want to delete \"%conf_name\" conference", array('%conf_name' => $form_tmp['conf_name']['#default_value'])),
    '#prefix' => '<div id="conf_name">',
    '#suffix' => '</div>',
  );

  $form['conf_name']           = array('#type' => 'value', '#value'  => $form_tmp['conf_name']['#default_value']);
  $form['conf_type']           = array('#type' => 'value', '#value'  => $form_tmp['conf_type']['#default_value']);
  $form['conf_layout']         = array('#type' => 'value', '#value'  => $form_tmp['conf_layout']['#default_value']);
  $form['conf_start_date']     = array('#type' => 'value', '#value'  => $form_tmp['conf_start_date']['#default_value']);
  $form['conf_duration']       = array('#type' => 'value', '#value'  => $form_tmp['conf_duration']['#default_value']);
  $form['conf_transfer_rate']  = array('#type' => 'value', '#value'  => $form_tmp['advanced']['conf_transfer_rate']['#default_value']);
  $form['AES']                 = array('#type' => 'value', '#value'  => $form_tmp['advanced']['AES']['#default_value']);
  $form['DTMF']                = array('#type' => 'value', '#value'  => $form_tmp['advanced']['DTMF']['#default_value']);
  $form['advanced_layout']     = array('#type' => 'value', '#value'  => $form_tmp['advanced']['advanced_layout']['#default_value']);
  $form['conf_archive_stream'] = array('#type' => 'value', '#value'  => $form_tmp['advanced']['conf_archive']['#default_value']);
  $form['conf_id']             = array('#type' => 'value', '#value'  => arg(1));
  $form['cancel']              = array('#type' => 'submit', '#value' => t('Cancel'));
  $form['mcu_delete']          = array('#type' => 'submit', '#value' => t('Delete'));

  return $form;
}

function _mcu_reservation_delete_form_submit($form, &$form_state) {
  global $_mcu_booking_mcu, $user, $_mcu_booking_web_user_id, $_mcu_booking_web_user_name, $_mcu_booking_web_user_uname, $_mcu_booking_web_user_org;
  $v = $form_state['values'];
  $cid = $v['conf_id'];

  if ($form_state['values']['op'] !== t('Cancel')) {
    $result = $_mcu_booking_mcu->mcu_reservation_delete($cid);
    $is_success = $result->getELementsByTagName('ID')->item(0)->textContent;

    if ($is_success === '0') {
      //log into the db the success reservation data
      _res_data_log_db($v, 'deleted');

      //send an email to the user and the administrator
      _send_mail($v, 'delete_reservation');

      drupal_set_message(t("%conf_short_name conference was remove.", array('%conf_short_name' => $v['conf_name'])));
      watchdog('mcu_booking', '#!cid !conf_short_name conference was remove.', array('!cid' => $cid, '!conf_short_name' => $v['conf_name']), WATCHDOG_INFO);
      drupal_goto('mcu_booking');
    }
    else {
      $error = _error_handling(&$result);
      drupal_set_message($error['error'], 'error');
      drupal_goto('mcu_booking');
    }
  }
  else {
    drupal_goto('mcu_booking');
  }
}

function _mcu_reservation_get_details($conf_id) {
  global $_mcu_booking_mcu, $_mcu_booking_module_path;

  $details = $_mcu_booking_mcu->mcu_reservation_get_details($conf_id);
  if (!empty($details['id'])) {
    $details['layout'] = theme_image($_mcu_booking_module_path .'/images/'. check_plain($details['layout']) .'.jpg', check_plain($details['layout']));
    return theme('conf_details', $details);
  }
  else {
    drupal_goto('ongoing_details/'. $conf_id .'/view/');
  }
}

function _mcu_reservation_modify($form_state = NULL) {
  global $_mcu_booking_mcu;
  $conf_id = arg(1);
  $res_data = $_mcu_booking_mcu->mcu_reservation_get_details($conf_id);
  $form = _mcu_reservation_form();

  $form['name'] = array(
    '#type'   => 'value',
    '#value'  => $res_data['name']
  );

  $form['web_user_id'] = array(
    '#type'   => 'value',
    '#value'  => $res_data['web_reserved_uid']
  );

  $form['db_id'] = array(
    '#type'   => 'value',
    '#value'  => $res_data['web_db_id']
  );

  $form['conf_name']['#default_value'] = $res_data['display_name'];

  if ($res_data['layout'] === '1x1') {
    $conf_type = 'transcoding';
  }
  else {
    $conf_type = 'continuous_presence';
  }
  $form['conf_type']['#default_value'] = $conf_type;
  $form['conf_layout']['#default_value'] = $res_data['layout'];
  $form['conf_start_date']['#default_value'] = date('Y-m-d H:i:s', $res_data['start_time']);
  $form['conf_duration']['#default_value'] = str_replace('00', '0', $res_data['duration']);
  $form['conf_participants']['#default_value'] = $res_data['min_num_of_participant'];

  if ($res_data['transfer_rate'] != '4096' OR $res_data['encryption'] OR $res_data['DTMF']  OR !empty($res_data['advanced_layout']) OR $res_data['conf_archive_stream']) {
    $form['advanced']['#collapsed'] = FALSE;
  }
  $form['advanced']['conf_transfer_rate']['#default_value'] = $res_data['transfer_rate'];
  $form['advanced']['AES']['#default_value'] = $res_data['encryption'];
  $form['advanced']['DTMF']['#default_value'] = $res_data['DTMF'];
  if ($res_data['same_layout'] === 'true' AND $res_data['legacy'] === 'false' AND $res_data['lecture_mode'] === 'false') {
    $form['advanced']['advanced_layout']['#default_value'] = 1;
  }
  elseif ($res_data['same_layout'] === 'false' AND $res_data['legacy'] == 'false' AND $res_data['lecture_mode'] === 'true') {
    $form['advanced']['advanced_layout']['#default_value'] = 2;
  }
  elseif ($res_data['same_layout'] === 'false' AND $res_data['legacy'] == 'true' AND $res_data['lecture_mode'] === 'false') {
    $form['advanced']['advanced_layout']['#default_value'] = 3;
  }
  else {
    unset($form['advanced']['advanced_layout']['#default_value']);
  }
  $form['advanced']['conf_archive_stream']['#default_value'] = $res_data['vcr'];
  $form['submit']['#value'] = t('Modify');

  return $form;
}

function _mcu_reservation_modify_validate($form, &$form_state) {
  if (strtotime($form_state['values']['conf_start_date']) <= time()) {
      form_set_error('conf_start_date', t('Its not possible to book confernce in the past.'));
  }
}

function _mcu_reservation_modify_submit($form, &$form_state) {
  global $_mcu_booking_mcu;
  $conf_id = arg(1);
  $v = $form_state['values'];

  //add values
  $v['conf_type'] === 'voice_conference' ? $media = 'audio' : $media = 'video_audio';
  empty($v['conf_layout']) ? $v['conf_layout'] = '1x1' : NULL;

  if ($v['conf_archive_stream']) {
    $v['ipvcr_name'] = variable_get('mcu_booking_ipvcr_name', NULL);
    $v['ipvcr_e164'] = variable_get('mcu_booking_ipvcr_e164', NULL);
  }
  //update the reservation on the MCU
  $result = $_mcu_booking_mcu->mcu_reservation_update($v, $conf_id);

  $is_success = $result->getELementsByTagName('ID')->item(0)->textContent;
  if (is_string($is_success) && $is_success === '0') {
    $v['prefix']            = variable_get('mcu_booking_routing_prefix', NULL);
    $v['conf_number']       = $result->getElementsByTagName('NAME')->item(0)->textContent;
    $v['conf_id']           = $result->getElementsByTagName('ID')->item(1)->textContent;
    $v['conf_num_id']       = $result->getElementsByTagName('NUMERIC_ID')->item(0)->textContent;
    $v['conf_entry_pass']   = $result->getElementsByTagName('ENTRY_PASSWORD')->item(0)->textContent;
    $v['conf_chair_pass']   = $result->getElementsByTagName('LEADER_PASSWORD')->item(0)->textContent;

    //log into the db the success reservation data
    _res_data_log_db($v, 'update');

    //send an email to the user and the administrator
    _send_mail($v, 'modify_reservation');

    watchdog('mcu_booking', '#!cid !conf_short_name conference was modify.', array('!cid' => $v['conf_num_id'], '!conf_short_name' => $v['conf_name']), WATCHDOG_INFO);
    //redirect the browser to the reserved conference detail page
    drupal_goto('reservation_details/'. check_plain($v['conf_id']) .'/view');
  }
  else {
    $error = _error_handling(&$result);
    form_set_error($error['element'], $error['error'], FALSE);
    $form_state['rebuild'] = TRUE;
  }
}
