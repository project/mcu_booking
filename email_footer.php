    <p><?php print t('Please check conference parameters carefully. In case you find any of the parameters unsuitable, visit our booking system page at <a href="http://vvc.niif.hu/" alt="Collaboration portal of NIIF Institute">http://vvc.niif.hu/</a> and modify conference attributes.'); ?></p>

    <div id="footer">
      <p>
        <?php print t('NIIF Booking System'); ?><br />
        <?php print t('In case of any problem, please contact <a href="mailto:video-admin@niif.hu">video-admin@niif.hu</a>'); ?>
      </p>
    </div>
  </body>
</html>
