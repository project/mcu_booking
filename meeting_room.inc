<?php
// $Id$

/**
 * @file
 *   Meeting room part of the mcu booking UI.
 */
function _meeting_room_reservation($form_state = NULL) {
  global $_mcu_booking_mcu;
  $form = _mcu_booking_common_form();

  //delete unused items
  unset($form['conf_start_date']);
  unset($form['advanced']['conf_archive_stream']);

  $form['advanced']['#prefix'] = '<div style="clear: both;">';
  $form['advanced']['#suffix'] = '</div>';

  $form['conf_duration'] = array(
    '#type' => 'value',
    '#value' => '6:0',
  );

  $form['conf_name']['#title'] = t('Meeting room short name');

  $form['conf_type']['#title'] = t('Select meeting room type');

  return $form;
}

//TODO
function _meeting_room_reservation_validate($form, &$form_state) {
}

function _meeting_room_reservation_submit($form, &$form_state) {
  global $_mcu_booking_mcu, $_mcu_booking_web_user_uname, $_mcu_booking_web_user_id;

  //create and clean the data array
  $v = $form_state['values'];

  //add values
  $v['web_user_id'] = $_mcu_booking_web_user_id;
  !empty($_mcu_booking_web_user_uname) ? $v['web_user_uname'] = $_mcu_booking_web_user_uname : $v['web_user_uname'] = 'anonymous';
  $v['conf_type'] === 'voice_conference'?$media = 'audio':$media = 'video_audio';
  empty($v['conf_layout']) ? $v['conf_layout']='1x1' : NULL;

  if ($v['conf_archive_stream']) {
    $v['ipvcr_name'] = variable_get('mcu_booking_ipvcr_name', NULL);
    $v['ipvcr_e164'] = variable_get('mcu_booking_ipvcr_e164', NULL);
  }

  //create the meeting room reservation on the MCU
  $result = $_mcu_booking_mcu->mcu_reservation_create($v, TRUE);

  //Check if the meeting room reservation was successful
  $is_success = $result->getELementsByTagName('ID')->item(0)->textContent;
  if (is_string($is_success) && $is_success === '0') {
    $v['prefix']            = $result->getElementsByTagName('PREFIX')->item(0)->textContent;
    $v['conf_number']       = $result->getElementsByTagName('NAME')->item(0)->textContent;
    $v['conf_id']           = $result->getElementsByTagName('ID')->item(1)->textContent;
    $v['conf_num_id']       = $result->getElementsByTagName('NUMERIC_ID')->item(0)->textContent;
    $v['conf_entry_pass']   = $result->getElementsByTagName('ENTRY_PASSWORD')->item(0)->textContent;
    $v['conf_chair_pass']   = $result->getElementsByTagName('LEADER_PASSWORD')->item(0)->textContent;

    //log into the db the success reservation data
    _res_data_log_db($v);

    //send an email to the user and the administrator
    _send_mail($v, 'create_meeting_room');

    watchdog('mcu_booking', '#!cid !conf_short_name meeting room was created.', array('!cid' => $v['conf_num_id'], '!conf_short_name' => $v['conf_name']), WATCHDOG_INFO);
    //redirect the browser to the reserved conference detail page
    drupal_goto('meeting_room_details/'. check_plain($v['conf_id']));
  }
  else {
    $error = _error_handling(&$result);
    form_set_error($error['element'], $error['error'], FALSE);
    $form_state['rebuild'] = TRUE;
  }
}

function _mcu_meeting_room_delete_form($form_state = NULL) {
  $form = array();
  $form_tmp = _mcu_meeting_room_modify();

  $form['displpay_conf_name'] = array(
    '#type'   => 'item',
    '#value'  => t("Do you really want to delete \"%conf_name\" meeting room?", array('%conf_name' => $form_tmp['conf_name']['#default_value'])),
    '#prefix' => '<div id="conf_name">',
    '#suffix' => '</div>',
  );

  $form['conf_name']           = array('#type' => 'value', '#value'  => $form_tmp['conf_name']['#default_value']);
  $form['conf_type']           = array('#type' => 'value', '#value'  => $form_tmp['conf_type']['#default_value']);
  $form['conf_layout']         = array('#type' => 'value', '#value'  => $form_tmp['conf_layout']['#default_value']);
  $form['conf_start_date']     = array('#type' => 'value', '#value'  => $form_tmp['conf_start_date']['#default_value']);
  $form['conf_duration']       = array('#type' => 'value', '#value'  => $form_tmp['conf_duration']['#default_value']);
  $form['conf_transfer_rate']  = array('#type' => 'value', '#value'  => $form_tmp['advanced']['conf_transfer_rate']['#default_value']);
  $form['AES']                 = array('#type' => 'value', '#value'  => $form_tmp['advanced']['AES']['#default_value']);
  $form['DTMF']                = array('#type' => 'value', '#value'  => $form_tmp['advanced']['DTMF']['#default_value']);
  $form['advanced_layout']     = array('#type' => 'value', '#value'  => $form_tmp['advanced']['advanced_layout']['#default_value']);
  $form['conf_archive_stream'] = array('#type' => 'value', '#value'  => $form_tmp['advanced']['conf_archive']['#default_value']);
  $form['conf_id'] = array('#type' => 'value', '#value' => arg(1));
  $form['cancel'] = array('#type' => 'submit', '#value' =>  t('Cancel'));
  $form['mcu_delete'] = array('#type' => 'submit', '#value' => t('Delete'));

  return $form;
}

function _mcu_meeting_room_delete_form_submit($form, &$form_state) {
  global $_mcu_booking_mcu;
  $v = $form_state['values'];

  if ($form_state['values']['op'] !== t('Cancel')) {
    $result = $_mcu_booking_mcu->mcu_meeting_room_terminate($v['conf_id']);

    //Check if the meeting room reservation was successful
    $is_success = $result->getELementsByTagName('ID')->item(0)->textContent;
    if ($is_success === '65538') {
      $v['prefix']            = $result->getElementsByTagName('PREFIX')->item(0)->textContent;
      $v['conf_number']       = $result->getElementsByTagName('NAME')->item(0)->textContent;
      $v['conf_id']           = $result->getElementsByTagName('ID')->item(1)->textContent;
      $v['conf_num_id']       = $result->getElementsByTagName('NUMERIC_ID')->item(0)->textContent;
      $v['conf_entry_pass']   = $result->getElementsByTagName('ENTRY_PASSWORD')->item(0)->textContent;
      $v['conf_chair_pass']   = $result->getElementsByTagName('LEADER_PASSWORD')->item(0)->textContent;

      //log into the db the success reservation data
      _res_data_log_db($v);

      //send an email to the user and the administrator
      _send_mail($v, 'terminate_meeting_room');

      $variables = array('!cid' => $v['conf_num_id'], '!conf_short_name' => $v['conf_name']);
      drupal_set_message(t('#!cid !conf_short_name meeting room was delete.', $variables));
      watchdog('mcu_booking', '#!cid !conf_short_name meeting room was delete.', $variables, WATCHDOG_INFO);

      //redirect the browser to the main page
      drupal_goto('mcu_booking');
    }
    else {
      $error = _error_handling(&$result);
      drupal_set_message($error['error'], 'error');
      drupal_goto('mcu_booking');
    }
  }
  else {
    drupal_goto('mcu_booking');
  }
}

function _mcu_meeting_room_get_details($conf_id) {
  global $_mcu_booking_mcu, $_mcu_booking_module_path;

  $details = $_mcu_booking_mcu->mcu_meeting_room_get_details($conf_id);
  $details['layout'] = theme_image($_mcu_booking_module_path .'/images/'. check_plain($details['layout']) .'.gif', check_plain($details['layout']));

  return theme('conf_details', $details);
}

function _mcu_meeting_room_modify($form_state = NULL) {
  global $_mcu_booking_mcu;
  $conf_id = arg(1);
  $res_data = $_mcu_booking_mcu->mcu_meeting_room_get_details($conf_id);

  require_once('reservation.inc');
  $form = _meeting_room_reservation();

  $form['name'] = array(
    '#type'   => 'value',
    '#value'  => $res_data['name']
  );

  $form['web_user_id'] = array(
    '#type'   => 'value',
    '#value'  => $res_data['web_reserved_uid']
  );

  $form['db_id'] = array(
    '#type'   => 'value',
    '#value'  => $res_data['web_db_id']
  );

  $form['conf_name']['#default_value'] = $res_data['display_name'];
  if ($res_data['layout'] === '1x1') {
    $conf_type = 'transcoding';
  }
  else {
    $conf_type = 'continuous_presence';
  }
  $form['conf_type']['#default_value'] = $conf_type;
  $form['conf_layout']['#default_value'] = $res_data['layout'];
  $form['conf_start_date']['#default_value'] = date('Y-m-d H:i:s', $res_data['start_time']);
  $form['conf_duration']['#default_value'] = $res_data['duration'];
  $form['conf_participants']['#default_value'] = $res_data['min_num_of_participant'];
  if ($res_data['transfer_rate'] != '4092' OR $res_data['encryption'] OR $res_data['DTMF']  OR !empty($res_data['advanced_layout']) OR $res_data['conf_archive_stream']) {
    $form['advanced']['#collapsed'] = FALSE;
  }
  $form['advanced']['conf_transfer_rate']['#default_value'] = $res_data['transfer_rate'];
  $form['advanced']['AES']['#default_value'] = $res_data['encryption'];
  $form['advanced']['DTMF']['#default_value'] = $res_data['DTMF'];
  if ($res_data['same_layout'] === 'true' AND $res_data['lecture_mode'] === 'false') {
    $form['advanced']['advanced_layout']['#default_value'] = 1;
  }
  if ($res_data['same_layout'] === 'false' AND $res_data['lecture_mode'] === 'true') {
    $form['advanced']['advanced_layout']['#default_value'] = 2;
  }
  $form['advanced']['conf_archive_stream']['#default_value'] = $res_data['vcr'];
  $form['submit']['#value'] = t('Modify');

  return $form;
}

//TODO
function _mcu_meeting_room_modify_validate($form, &$form_state) {}

function _mcu_meeting_room_modify_submit($form, &$form_state) {
  global $_mcu_booking_mcu, $_mcu_booking_web_user_uname;
  $conf_id = arg(1);

  //create and clean the data array
  $v = $form_state['values'];

  //add values
  !empty($_mcu_booking_web_user_uname) ? $v['web_user_uname'] = $_mcu_booking_web_user_uname : $v['web_user_uname'] = 'anonymous';
  $v['conf_type'] === 'voice_conference' ? $media = 'audio' : $media = 'video_audio';
  empty($v['conf_layout']) ? $v['conf_layout'] = '1x1' : NULL;

  //add VCR participant values
  if ($v['conf_archive_stream']) {
    $v['ipvcr_name'] = variable_get('mcu_booking_ipvcr_name', NULL);
    $v['ipvcr_e164'] = variable_get('mcu_booking_ipvcr_e164', NULL);
  }

  //update the reservation on the MCU
  $result = $_mcu_booking_mcu->mcu_reservation_update($v, $conf_id, TRUE);

  //Check if the meeting room reservation was successful
  $is_success = $result->getELementsByTagName('ID')->item(0)->textContent;
  if (is_string($is_success) && $is_success === '0') {
    $v['prefix']            = $result->getElementsByTagName('PREFIX')->item(0)->textContent;
    $v['conf_number']       = $result->getElementsByTagName('NAME')->item(0)->textContent;
    $v['conf_id']           = $result->getElementsByTagName('ID')->item(1)->textContent;
    $v['conf_num_id']       = $result->getElementsByTagName('NUMERIC_ID')->item(0)->textContent;
    $v['conf_entry_pass']   = $result->getElementsByTagName('ENTRY_PASSWORD')->item(0)->textContent;
    $v['conf_chair_pass']   = $result->getElementsByTagName('LEADER_PASSWORD')->item(0)->textContent;

    //log into the db the success reservation data
    _res_data_log_db($v);

    //send an email to the user and the administrator
    _send_mail($v, 'modify_meeting_room');

    watchdog('mcu_booking', '#!cid !conf_short_name meeting room was created.', array('!cid' => $v['conf_num_id'], '!conf_short_name' => $v['conf_name']), WATCHDOG_INFO);
    //redirect the browser to the reserved conference detail page
    drupal_goto('meeting_room_details/'. check_plain($v['conf_id']) .'/view');
  }
  else {
    $error = _error_handling(&$result);
    form_set_error($error['element'], $error['error'], FALSE);
    $form_state['rebuild'] = TRUE;
  }
}
