$(document).ready(function() {
  //help texts
  $('.question').click(function () {
  $x = $(this).parent().parent().attr("for");
  if ($x == '') {
    $x = $(this).parent().parent().next().children().children().children().attr("name");
    $x = 'edit-'+ $x;
    $x = $x.replace('_', '-');
  }

  $dialog = $('<div id="edit-conf-type-helper"></div>')
                .html($("#"+ $x  +"-help").attr("value"))
                .dialog({
                  autoOpen: false,
                  dialogClass: 'help',
                  height: 400,
                  width: 600,
                  closeText: '(ESC) X',
                  modal: true,
                });

    $dialog.dialog('open');
  });
});
